kevinoid's Git Configuration
============================

This repository contains [kevinoid](https://gitlab.com/kevinoid/gitconfig)'s
git configuration, including hook scripts for running global (i.e.
`~/.config/git/hooks`), repo-specific uncommitted (i.e.  `.git/hooks`), and
repo-specific committed (i.e. `.githooks`) scripts.  It references
[githooks](https://gitlab.com/rycus86/githooks.git) as a submodule for ease of
use and updating (which avoids the need to use githooks' auto-update feature).

To use this git configuration, run:

```sh
git clone --recurse-submodules https://github.com/kevinoid/gitconfig.git ~/.config/git
```

Then edit `~/.config/git/config` as desired.
