#!/bin/sh

# An example hook script to verify what is about to be pushed.  Called by "git
# push" after it has checked the remote status, but before anything has been
# pushed.  If this script exits with a non-zero status nothing will be pushed.
#
# This hook is called with the following parameters:
#
# $1 -- Name of the remote to which the push is being done
# $2 -- URL to which the push is being done
#
# If pushing without using a named remote those arguments will be equal.
#
# Information about the commits which are being pushed is supplied as lines to
# the standard input in the form:
#
#   <local ref> <local sha1> <remote ref> <remote sha1>
#
# This hook prevents pushing commits where the commit summary starts with
# WIP (case-insensitive, indicating a work-in-progress) or "fixup!" or
# "squash!" (likely indicating a forgotten rebase --autosquash).
#
# Based on:
# https://github.com/git/git/blob/v2.26.2/templates/hooks--pre-push.sample

# shellcheck disable=2034

set -Ceu

remote="$1"
url="$2"

z40=0000000000000000000000000000000000000000

# Read local refs into argument array
set --
while read -r local_ref local_sha remote_ref remote_sha; do
	if [ "$local_sha" != $z40 ]; then
		set -- "$@" "$local_ref"
	fi
done

# Exit if no local refs to push
if [ "$#" -eq 0 ]; then
	exit 0
fi

# Check for WIP commits in local refs not in remote
if wip_commits=$(git log --oneline "$@" --not --remotes="$remote" |
	grep -E '^[[:xdigit:]]+[[:space:]]+(fixup!|squash!|[Ww][Ii][Pp]($|[^[:alnum:]]))'); then
	echo "Refusing to push commits with summary indicating work in progress:
$wip_commits"
	exit 1
fi

exit 0
